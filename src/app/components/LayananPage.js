import Image from "next/image";
import Link from "next/link";

const LayananPage = () => {
    const productItem = [
        {
            productId: 40,
            productName: "x-Smartz",
            productDesc: "Sistem Informasi Manajemen Pemerintahan Cloud",
            isPlaystoreAvailable: false,
            productLinkDonwload: "",
            productImage: "/slide_4.jpg",
            productImageSize: {
                width: 100,
                height: 200,
            },
        },
    ];
    return (
<div className="flex flex-col lg:flex-row min-h-screen lg:h-screen overflow-hidden">

    <div
      className="relative bg-blue-100 p-5 flex flex-col justify-end lg:items-center gap-2 lg:pb-20 lg:text-center group h-60 lg:h-full lg:w-1/3 lg:hover:w-2/5 transition-[width] bg-cover bg-center"
      style={{ backgroundImage: "url('/system_integrator.jpeg')" }}
    >
      <div className="absolute inset-0 bg-black opacity-90 group-hover:opacity-50 transition-opacity duration-200"></div>
      <h3 className="relative text-white text-lg font-semibold lg:group-hover:-translate-y-8 transition duration-200">System Integrator</h3>
      <p className="relative text-white lg:w-80 text-sm lg:group-hover:-translate-y-8 transition duration-200">
      Kami membantu proses penggabungan berbagai subsistem komputasi dan perangkat lunak yang berbeda menjadi satu sistem besar yang berfungsi sebagai satu unit. Hal ini dilakukan untuk meningkatkan efisiensi operasional, mengurangi biaya, dan meningkatkan produktivitas.
      </p>
      {/* <Link href='/details'>
        <button className="relative text-white h-10 w-40 px-5 border border-white mt-2 rounded-full lg:opacity-0 lg:group-hover:opacity-100 transition duration-200 lg:group-hover:-translate-y-2 hover:bg-white hover:text-black">
          selengkapnya...
        </button>
      </Link> */}
    </div>
    <div
      className="relative bg-blue-100 p-5 flex flex-col justify-end lg:items-center gap-2 lg:pb-20 lg:text-center group h-60 lg:h-full lg:w-1/3 lg:hover:w-2/5 transition-[width] bg-cover bg-center"
      style={{ backgroundImage: "url('/mobile-apps.jpg')" }}
    >
      <div className="absolute inset-0 bg-black opacity-90 group-hover:opacity-50 transition-opacity duration-200"></div>
      <h3 className="relative text-white text-lg font-semibold lg:group-hover:-translate-y-8 transition duration-200">Mobile/Web Apps Development</h3>
      <p className="relative text-white lg:w-80 text-sm lg:group-hover:-translate-y-8 transition duration-200">
      Pengembangan Aplikasi Mobile/Web melibatkan pembuatan aplikasi yang dapat berjalan di perangkat mobile (seperti smartphone dan tablet) dan platform web. Ini adalah bidang penting dalam era digital saat ini karena meningkatnya penggunaan perangkat mobile dan akses internet.
      </p>
      {/* <Link href='/details'>
        <button className="relative text-white h-10 w-40 px-5 border border-white mt-2 rounded-full lg:opacity-0 lg:group-hover:opacity-100 transition duration-200 lg:group-hover:-translate-y-2 hover:bg-white hover:text-black">
          selengkapnya...
        </button>
      </Link> */}
    </div>
    <div
      className="relative bg-blue-100 p-5 flex flex-col justify-end lg:items-center gap-2 lg:pb-20 lg:text-center group h-60 lg:h-full lg:w-1/3 lg:hover:w-2/5 transition-[width] bg-cover bg-center"
      style={{ backgroundImage: "url('/it-infra.jpg')" }}
    >
      <div className="absolute inset-0 bg-black opacity-90 group-hover:opacity-50 transition-opacity duration-200"></div>
      <h3 className="relative text-white text-lg font-semibold lg:group-hover:-translate-y-8 transition duration-200">IT Infrastruktur</h3>
      <p className="relative text-white lg:w-80 text-sm lg:group-hover:-translate-y-8 transition duration-200">
        Kami membangun komponen teknologi yang digunakan untuk mengelola dan mengoperasikan layanan IT, termasuk perangkat keras, perangkat lunak, jaringan, dan fasilitas data center. Infrastruktur IT yang solid adalah fondasi bagi operasional bisnis yang efisien dan andal.
      </p>
      {/* <Link href='/details'>
        <button className="relative text-white h-10 w-40 px-5 border border-white mt-2 rounded-full lg:opacity-0 lg:group-hover:opacity-100 transition duration-200 lg:group-hover:-translate-y-2 hover:bg-white hover:text-black">
          selengkapnya...
        </button>
      </Link> */}
    </div>
</div>

    
    );
};

export default LayananPage