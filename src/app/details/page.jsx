import Link from "next/link";
import { IoMdArrowBack } from "react-icons/io";

export default function Details() {
    return (
        <>
            <section className="flex flex-col-reverse gap-5 lg:flex-row max-w-screen-lg m-auto text-slate-700">
                <div className="lg:w-1/2 px-5 lg:px-0">
                    <Link href="/" className="flex items-center w-20 py-5 gap-2 text-slate-900 absolute lg:static top-0"><IoMdArrowBack /> Back</Link>
                    <div className="flex flex-col gap-2 pb-8">
                        <h3 className="text-xl font-bold text-slate-800">Tokopakedi</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas, provident. Laudantium est voluptatum reprehenderit ea earum ipsa dolores suscipit? Provident deleniti molestiae natus non ipsum quod, dolore cum quis placeat.</p>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ab nemo aliquam autem quo, laboriosam perspiciatis molestiae impedit? Aliquid aspernatur velit soluta asperiores facere atque vero fugit odio nulla. Animi quia consequatur nihil sint natus quasi veniam fuga, dolorem voluptatem nemo ipsam molestias sed saepe doloribus, a quidem dignissimos quas! Ipsam.</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas, provident. Laudantium est voluptatum reprehenderit ea earum ipsa dolores suscipit? Provident deleniti molestiae natus non ipsum quod, dolore cum quis placeat.</p>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ab nemo aliquam autem quo, laboriosam perspiciatis molestiae impedit? Aliquid aspernatur velit soluta asperiores facere atque vero fugit odio nulla. Animi quia consequatur nihil sint natus quasi veniam fuga, dolorem voluptatem nemo ipsam molestias sed saepe doloribus, a quidem dignissimos quas! Ipsam.</p>
                    </div>
                </div>
                <div className="lg:w-1/2 bg-orange-500 flex justify-center items-center h-80 lg:h-auto">
                    <p className="-rotate-45">disini gambar</p>
                </div>
            </section>

            <section className="py-8 bg-slate-200">
                <div className="max-w-screen-lg m-auto text-slate-700">
                    <h3 className="text-xl font-bold text-slate-800 text-center w-full">Layanan</h3>
                    <div className="grid grid-cols-1 lg:grid-cols-2 mt-8 gap-5">

                        <div className="flex bg-white rounded overflow-hidden">
                            <div className="w-1/4 p-5">
                                <div className="w-full h-full bg-orange-500 rounded-full"></div>
                            </div>
                            <div className="w-3/4 py-6 px-2 flex flex-col gap-2">
                                <h4 className="text-slate-800 text-lg font-semibold">Lorem</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                        </div>
                        <div className="flex bg-white rounded overflow-hidden">
                            <div className="w-1/4 p-5">
                                <div className="w-full h-full bg-orange-500 rounded-full"></div>
                            </div>
                            <div className="w-3/4 py-6 px-2 flex flex-col gap-2">
                                <h4 className="text-slate-800 text-lg font-semibold">Lorem</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                        </div>
                        <div className="flex bg-white rounded overflow-hidden">
                            <div className="w-1/4 p-5">
                                <div className="w-full h-full bg-orange-500 rounded-full"></div>
                            </div>
                            <div className="w-3/4 py-6 px-2 flex flex-col gap-2">
                                <h4 className="text-slate-800 text-lg font-semibold">Lorem</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                        </div>
                        <div className="flex bg-white rounded overflow-hidden">
                            <div className="w-1/4 p-5">
                                <div className="w-full h-full bg-orange-500 rounded-full"></div>
                            </div>
                            <div className="w-3/4 py-6 px-2 flex flex-col gap-2">
                                <h4 className="text-slate-800 text-lg font-semibold">Lorem</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                        </div>
                        <div className="flex bg-white rounded overflow-hidden">
                            <div className="w-1/4 p-5">
                                <div className="w-full h-full bg-orange-500 rounded-full"></div>
                            </div>
                            <div className="w-3/4 py-6 px-2 flex flex-col gap-2">
                                <h4 className="text-slate-800 text-lg font-semibold">Lorem</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section className="py-8 bg-slate-200">
                <div className="max-w-screen-lg m-auto text-slate-700">
                    <h3 className="text-xl font-bold text-slate-800 text-center w-full">Logistik</h3>
                    <div className="grid grid-cols-1 lg:grid-cols-2 mt-8 gap-5">

                        <div className="flex bg-white rounded overflow-hidden">
                            <div className="w-1/4 p-5">
                                <div className="w-full h-full bg-orange-500 rounded-full"></div>
                            </div>
                            <div className="w-3/4 py-6 px-2 flex flex-col gap-2">
                                <h4 className="text-slate-800 text-lg font-semibold">Lorem</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                        </div>
                        <div className="flex bg-white rounded overflow-hidden">
                            <div className="w-1/4 p-5">
                                <div className="w-full h-full bg-orange-500 rounded-full"></div>
                            </div>
                            <div className="w-3/4 py-6 px-2 flex flex-col gap-2">
                                <h4 className="text-slate-800 text-lg font-semibold">Lorem</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                        </div>
                        <div className="flex bg-white rounded overflow-hidden">
                            <div className="w-1/4 p-5">
                                <div className="w-full h-full bg-orange-500 rounded-full"></div>
                            </div>
                            <div className="w-3/4 py-6 px-2 flex flex-col gap-2">
                                <h4 className="text-slate-800 text-lg font-semibold">Lorem</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                        </div>
                        <div className="flex bg-white rounded overflow-hidden">
                            <div className="w-1/4 p-5">
                                <div className="w-full h-full bg-orange-500 rounded-full"></div>
                            </div>
                            <div className="w-3/4 py-6 px-2 flex flex-col gap-2">
                                <h4 className="text-slate-800 text-lg font-semibold">Lorem</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                        </div>
                        <div className="flex bg-white rounded overflow-hidden">
                            <div className="w-1/4 p-5">
                                <div className="w-full h-full bg-orange-500 rounded-full"></div>
                            </div>
                            <div className="w-3/4 py-6 px-2 flex flex-col gap-2">
                                <h4 className="text-slate-800 text-lg font-semibold">Lorem</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section className="grid md:grid-cols-2 max-w-screen-2xl m-auto">
                <div className="h-96 bg-orange-500"></div>
                <div className="flex flex-col justify-center p-5 gap-4">
                    <h2 className="text-2xl font-bold text-slate-800 w-full">Penutup</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam assumenda, saepe dignissimos voluptas eveniet doloribus! Provident delectus, eius beatae distinctio recusandae quos illo, adipisci ipsa voluptatem iusto corrupti vel accusamus.</p>
                </div>
            </section>

            <section className="min-h-screen bg-slate-900">
            </section>
        </>
    )
}